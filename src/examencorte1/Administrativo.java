/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package examencorte1;

/**
 *
 * @author ramir
 */
public class Administrativo extends Empleados implements isr {
    
    private String empleadobase;
    private String empleadoeventual;
    
    // Constructor que inicializa los campos específicos de Administrativo con valores proporcionados
    public Administrativo(int numEmpleado, String nombre, String domicilio, int pagopordia, int diastrabajados, int isr, String empleadobase, String empleadoeventual) {
        super(numEmpleado, nombre, domicilio, pagopordia, diastrabajados, isr);
        this.empleadobase = empleadobase;
        this.empleadoeventual = empleadoeventual;
    }

    // Constructor que inicializa los campos específicos de Administrativo a cadenas vacías
    public Administrativo(int numEmpleado, String nombre, String domicilio, int pagopordia, int diastrabajados, int isr) {
        super(numEmpleado, nombre, domicilio, pagopordia, diastrabajados, isr);
        this.empleadobase = "";
        this.empleadoeventual = "";
    }

    public String getEmpleadobase() {
        return empleadobase;
    }

    public void setEmpleadobase(String empleadobase) {
        this.empleadobase = empleadobase;
    }

    public String getEmpleadoeventual() {
        return empleadoeventual;
    }

    public void setEmpleadoeventual(String empleadoeventual) {
        this.empleadoeventual = empleadoeventual;
    }
    
    @Override
    public float calculardescuento() {
        // Implementación de cálculo de descuento del ISR
        int salarioBruto = pagopordia * diastrabajados;
        return (float) (salarioBruto * (isr / 100.0));
    }

    @Override
    public float totalapagar() {
        // Implementación de cálculo del total a pagar después del descuento del ISR
        int salarioBruto = pagopordia * diastrabajados;
        return salarioBruto - calculardescuento();
    }

    AbstractStringBuilder getNivel() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    AbstractStringBuilder getNivel() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}