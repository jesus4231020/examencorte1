/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package examencorte1;

/**
 *
 * @author ramir
 */
public class Empleados {

    static void add(Administrativo nuevoEmpleado) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    static boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    protected int numEmpleado;
    protected String nombre;
    protected String domicilio;
    protected int pagopordia;
    protected int diastrabajados;
    protected int isr; // Porcentaje de ISR

    public Empleados(int numEmpleado, String nombre, String domicilio, int pagopordia, int diastrabajados, int isr) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.pagopordia = pagopordia;
        this.diastrabajados = diastrabajados;
        this.isr = isr;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getPagopordia() {
        return pagopordia;
    }

    public void setPagopordia(int pagopordia) {
        this.pagopordia = pagopordia;
    }

    public int getDiastrabajados() {
        return diastrabajados;
    }

    public void setDiastrabajados(int diastrabajados) {
        this.diastrabajados = diastrabajados;
    }

    public int getIsr() {
        return isr;
    }

    public void setIsr(int isr) {
        this.isr = isr;
    }

    public double calcularSalario() {
        int salarioBruto = pagopordia * diastrabajados;
        double deduccionISR = salarioBruto * (isr / 100.0);
        return salarioBruto - deduccionISR;
    }
}