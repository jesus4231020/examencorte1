/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package examencorte1;

/**
 *
 * @author ramir
 */
public class Docente extends Empleados {
    
    private String prescolar;
    private String primaria;
    private String secundaria;

    // Constructor que inicializa todos los campos, incluyendo los específicos de Docente
    public Docente(int numEmpleado, String nombre, String domicilio, int pagopordia, int diastrabajados, int isr, String prescolar, String primaria, String secundaria) {
        super(numEmpleado, nombre, domicilio, pagopordia, diastrabajados, isr);
        this.prescolar = prescolar;
        this.primaria = primaria;
        this.secundaria = secundaria;
    }

    // Constructor que inicializa los campos específicos de Docente a cadenas vacías
    public Docente(String prescolar, String primaria, String secundaria, int numEmpleado, String nombre, String domicilio, int pagopordia, int diastrabajados, int isr) {
        super(numEmpleado, nombre, domicilio, pagopordia, diastrabajados, isr);
        this.prescolar = "";
        this.primaria = "";
        this.secundaria = "";
    }

    // Getters y setters para los campos específicos de Docente
    public String getPrescolar() {
        return prescolar;
    }

    public void setPrescolar(String prescolar) {
        this.prescolar = prescolar;
    }

    public String getPrimaria() {
        return primaria;
    }

    public void setPrimaria(String primaria) {
        this.primaria = primaria;
    }

    public String getSecundaria() {
        return secundaria;
    }

    public void setSecundaria(String secundaria) {
        this.secundaria = secundaria;
    }
}
