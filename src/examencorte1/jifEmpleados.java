/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package examencorte1;

import javax.swing.JOptionPane;

/**
 *
 * @author ramir
 */
public class jifEmpleados extends javax.swing.JPanel {

    /**
     * Creates new form jifEmpleados
     */
    public jifEmpleados() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtnumempleado = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        txtdomicilio = new javax.swing.JTextField();
        txtnivel = new javax.swing.JTextField();
        txtpagopordia = new javax.swing.JTextField();
        txtdiastrabajados = new javax.swing.JTextField();
        txtimpuesto = new javax.swing.JTextField();
        btnnuevo = new javax.swing.JButton();
        btnguardar = new javax.swing.JButton();
        btnmostrar = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtpago = new javax.swing.JTextField();
        txtpagoadicional = new javax.swing.JTextField();
        txttotal = new javax.swing.JTextField();
        txtimpuestos = new javax.swing.JTextField();
        txttotalapagar = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(102, 255, 255));
        jPanel1.setLayout(null);

        jLabel1.setText("NumEmpleado");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(6, 9, 80, 16);

        jLabel2.setText("Nombre");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(6, 49, 44, 16);

        jLabel3.setText("Domicilio");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(20, 120, 51, 16);

        jLabel4.setText("Nivel");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(30, 160, 27, 16);

        jLabel5.setText("PagoPorDia");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 210, 62, 16);

        jLabel6.setText("DiasTrabajados");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(10, 260, 79, 16);

        jLabel7.setText("Impuestos");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(6, 363, 55, 16);
        jPanel1.add(txtnumempleado);
        txtnumempleado.setBounds(104, 6, 64, 22);
        jPanel1.add(txtnombre);
        txtnombre.setBounds(104, 46, 64, 22);

        txtdomicilio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdomicilioActionPerformed(evt);
            }
        });
        jPanel1.add(txtdomicilio);
        txtdomicilio.setBounds(100, 120, 64, 22);

        txtnivel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnivelActionPerformed(evt);
            }
        });
        jPanel1.add(txtnivel);
        txtnivel.setBounds(100, 160, 64, 22);

        txtpagopordia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpagopordiaActionPerformed(evt);
            }
        });
        jPanel1.add(txtpagopordia);
        txtpagopordia.setBounds(100, 210, 64, 22);

        txtdiastrabajados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdiastrabajadosActionPerformed(evt);
            }
        });
        jPanel1.add(txtdiastrabajados);
        txtdiastrabajados.setBounds(100, 260, 64, 22);
        jPanel1.add(txtimpuesto);
        txtimpuesto.setBounds(104, 360, 64, 22);

        btnnuevo.setText("Nuevo");
        btnnuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnuevoActionPerformed(evt);
            }
        });
        jPanel1.add(btnnuevo);
        btnnuevo.setBounds(480, 120, 110, 94);

        btnguardar.setText("Guardar");
        btnguardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnguardarActionPerformed(evt);
            }
        });
        jPanel1.add(btnguardar);
        btnguardar.setBounds(480, 340, 113, 96);

        btnmostrar.setText("Mostrar");
        btnmostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmostrarActionPerformed(evt);
            }
        });
        jPanel1.add(btnmostrar);
        btnmostrar.setBounds(480, 230, 110, 95);

        jLabel8.setText("Pago");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(20, 440, 27, 16);

        jLabel9.setText("pago adicional al salario");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(20, 480, 127, 16);

        jLabel10.setText("total");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(20, 530, 24, 16);

        jLabel11.setText("impuestos");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(20, 560, 55, 16);

        jLabel12.setText("total pagar");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(20, 600, 57, 16);

        txtpago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpagoActionPerformed(evt);
            }
        });
        jPanel1.add(txtpago);
        txtpago.setBounds(170, 440, 64, 22);

        txtpagoadicional.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpagoadicionalActionPerformed(evt);
            }
        });
        jPanel1.add(txtpagoadicional);
        txtpagoadicional.setBounds(170, 480, 64, 22);

        txttotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotalActionPerformed(evt);
            }
        });
        jPanel1.add(txttotal);
        txttotal.setBounds(170, 520, 64, 22);
        jPanel1.add(txtimpuestos);
        txtimpuestos.setBounds(170, 550, 64, 22);

        txttotalapagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotalapagarActionPerformed(evt);
            }
        });
        jPanel1.add(txttotalapagar);
        txttotalapagar.setBounds(170, 600, 64, 22);

        jButton4.setText("Limpiar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);
        jButton4.setBounds(77, 670, 72, 23);

        jButton5.setText("Cancelar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton5);
        jButton5.setBounds(233, 670, 76, 23);

        jButton6.setText("Salir");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton6);
        jButton6.setBounds(412, 670, 72, 23);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtnivelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnivelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnivelActionPerformed

    private void btnmostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmostrarActionPerformed
        // TODO add your handling code here:
         // Verificar si hay empleados guardados
    if (Empleados.isEmpty()) {
        JOptionPane.showMessageDialog(this, "No hay empleados guardados.");
        return;
    }

    // Crear un StringBuilder para almacenar la información de los empleados
    StringBuilder infoEmpleados = new StringBuilder();
    infoEmpleados.append("Empleados guardados:\n\n");

    // Recorrer la lista de empleados y agregar sus datos al StringBuilder
    for (Administrativo empleado : Empleados) {
        infoEmpleados.append("NumEmpleado: ").append(empleado.getNumEmpleado()).append("\n");
        infoEmpleados.append("Nombre: ").append(empleado.getNombre()).append("\n");
        infoEmpleados.append("Domicilio: ").append(empleado.getDomicilio()).append("\n");
        infoEmpleados.append("Nivel: ").append(empleado.getNivel()).append("\n");
        infoEmpleados.append("PagoPorDia: ").append(empleado.getPagopordia()).append("\n");
        infoEmpleados.append("DiasTrabajados: ").append(empleado.getDiastrabajados()).append("\n");
        infoEmpleados.append("Impuestos: ").append(empleado.getIsr()).append("\n");
        infoEmpleados.append("--------------------------------------\n");
    }

    // Mostrar el cuadro de diálogo con la información de los empleados
    JOptionPane.showMessageDialog(this, infoEmpleados.toString());
    }//GEN-LAST:event_btnmostrarActionPerformed

    private void txttotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotalActionPerformed

    private void txttotalapagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotalapagarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotalapagarActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton5ActionPerformed

    private void txtpagopordiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpagopordiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpagopordiaActionPerformed

    private void txtdomicilioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdomicilioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdomicilioActionPerformed

    private void txtdiastrabajadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdiastrabajadosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdiastrabajadosActionPerformed

    private void txtpagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpagoActionPerformed

    private void txtpagoadicionalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpagoadicionalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpagoadicionalActionPerformed

    private void btnguardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnguardarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton6ActionPerformed

    private void btnnuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnuevoActionPerformed
        // TODO add your handling code here:
        // Obtener los datos ingresados en los campos de texto
    int numEmpleado = Integer.parseInt(txtnumempleado.getText());
    String nombre = txtnombre.getText();
    String domicilio = txtdomicilio.getText();
    int nivel = Integer.parseInt(txtnivel.getText());
    int pagopordia = Integer.parseInt(txtpagopordia.getText());
    int diastrabajados = Integer.parseInt(txtdiastrabajados.getText());
    int isr = Integer.parseInt(txtimpuesto.getText());

    // Crear un objeto Administrativo con los datos
    Administrativo nuevoEmpleado = new Administrativo(numEmpleado, nombre, domicilio, pagopordia, diastrabajados, isr);

    // Agregar el nuevo empleado a la lista
    Empleados.add(nuevoEmpleado);

    // Mostrar un mensaje de confirmación
    JOptionPane.showMessageDialog(this, "Empleado guardado exitosamente.");

    // Limpiar los campos de texto después de guardar
    txtnumempleado.setText("");
    txtnombre.setText("");
    txtdomicilio.setText("");
    txtnivel.setText("");
    txtpagopordia.setText("");
    txtdiastrabajados.setText("");
    txtimpuesto.setText("");
    }//GEN-LAST:event_btnnuevoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnguardar;
    private javax.swing.JButton btnmostrar;
    private javax.swing.JButton btnnuevo;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtdiastrabajados;
    private javax.swing.JTextField txtdomicilio;
    private javax.swing.JTextField txtimpuesto;
    private javax.swing.JTextField txtimpuestos;
    private javax.swing.JTextField txtnivel;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txtnumempleado;
    private javax.swing.JTextField txtpago;
    private javax.swing.JTextField txtpagoadicional;
    private javax.swing.JTextField txtpagopordia;
    private javax.swing.JTextField txttotal;
    private javax.swing.JTextField txttotalapagar;
    // End of variables declaration//GEN-END:variables
}
